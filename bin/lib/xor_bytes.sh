#!/bin/bash

#
# Usage: xor_bytes <a> <b>
#
# Example:
#   $ xor_bytes $'\xF0\x0F' $'\x0F\xF0' | hd
#   00000000  ff ff                                             |..|
#   00000002
#
# Dependencies:
#   sudo apt install xxd
#   . $JUNK_LIB/die.sh
#
function xor_bytes() {
  local a_bin=$1
  local b_bin=$2

  # First convert to text (hex).
  declare -a a
  declare -a b
  a=($(tr -d '\n' <<< "$a_bin" | xxd -g 1 | cut -c 11-57))
  b=($(tr -d '\n' <<< "$b_bin" | xxd -g 1 | cut -c 11-57))

  local len_a=${#a[*]}
  local len_b=${#b[*]}
  if [[ "$len_a" -ne "$len_b" ]]; then
    die "ERROR: xor_bytes was given two different length byte arrays."
  fi

  # Use bash's xor operator, one byte at a time. Build up xxd output for
  # "reversing" into binary in the next step.
  local result_xxd=
  let offset=0
  while [[ "$offset" -lt "$len_a" ]]; do
    if [[ "$((offset % 16))" == 0 ]]; then
      printf -v result_xxd "%s\n%08x:" "$result_xxd" "$offset"
    fi
    local a_byte="0x${a[$offset]}"
    local b_byte="0x${b[$offset]}"
    local result_decimal="$((a_byte ^ b_byte))"
    printf -v result_xxd "%s %02x" "$result_xxd" "$result_decimal"
    let offset++
  done

  # Back to binary.
  echo "$result_xxd" | xxd -r
}
