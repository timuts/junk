#!/bin/bash

#
# Outputs a random 32-bit number based on two samples of bash's 16-bit $RANDOM.
#
function rand_uint32() {
  local r1=$RANDOM
  local r2=$RANDOM
  echo $(( (r1 << 16) | r2 ))
}
