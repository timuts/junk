#!/bin/bash

#
# Send SIGTERM to specified processes, wait up the specified number of seconds
# (unless the processes exit sooner), then send SIGKILL.
#
# Usage: sigterm_then_kill <user | ""> <grace_period_seconds> <pid> ...
#
function sigterm_then_kill() {
  local user=$1; shift
  local grace_period=$1; shift
  local pids="$*"

  local kill=kill
  if [[ "$user" ]]; then
    kill="sudo -u $user kill"
  fi

  $kill $pids
  echo "Sent TERM signal to ${pids[*]}. Waiting up to $grace_period seconds."

  local start=$(printf '%(%s)T')
  local end=$((start + grace_period))
  while [[ "$(printf '%(%s)T')" -ge "$end" ]]; do
    kill -0 $pids &> /dev/null || break
    sleep 0.5
  done

  $kill -9 $pids
  echo "Sent KILL signal to $pids."
}
