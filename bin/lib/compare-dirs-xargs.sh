#!/bin/bash
#
# See ~/bin/compare-dirs
#

if [[ $# -lt 3 ]]; then
  echo "Usage: $0 <dir_a> <dir_b> <relpath_1> [<relpath_2> ...]" >&2
  exit 1
fi

dir_a=$1; shift
dir_b=$1; shift

for d in "$dir_a" "$dir_b"; do
  if ! [[ -d "$d" ]]; then
    echo "ERROR: Not a dir: $d" >&2
    exit 1
  fi
done

while [[ $# -gt 0 ]]; do
  echo

  relpath="${1#./}"
  f_a="$dir_a/$relpath"
  f_b="$dir_b/$relpath"

  mtime_a=NOT_FOUND
  mtime_b=NOT_FOUND
  hash_a=NOT_FOUND
  hash_b=NOT_FOUND

  if [[ -f "$f_a" ]]; then
    mtime_a=$(stat --format="%Y" "$f_a")
    hash_a=$(sha256sum < "$f_a")
  fi
  if [[ -f "$f_b" ]]; then
    mtime_b=$(stat --format="%Y" "$f_b")
    hash_b=$(sha256sum < "$f_b")
  fi

  found_diff=false
  if ( [[ "$hash_a" = "NOT_FOUND" ]] && [[ "$hash_b" != "NOT_FOUND" ]] ) || \
     ( [[ "$hash_b" = "NOT_FOUND" ]] && [[ "$hash_a" != "NOT_FOUND" ]] ); then
    echo "# existence mismatch"
    found_diff=true
  else
    if [[ "$mtime_a" -ne "$mtime_b" ]]; then
      echo "# mtime mismatch"
      found_diff=true
    fi
    if [[ "$hash_a" != "$hash_b" ]]; then
      echo "# hash mismatch"
      found_diff=true
    fi
  fi
  if [[ "$found_diff" != true ]]; then
    echo "# exact match"
  fi

  ls -l "$f_a" "$f_b"
  shift
done
