#!/bin/bash

. "$JUNK_LIB/sigterm_then_kill.sh"

#
# Kill the specified user's processes, if "yes" is entered on stdin.
#
# Dependencies:
#   sigterm_then_kill
#
function ask_to_kill_user() {
  local user=$1
  local pids=$(pgrep -U "$user")

  if [[ -z "$pids" ]]; then
    echo "No running processes for user '$user'."
    return 0
  fi

  echo
  ps -U "$user" wwfu

  local answer=
  while sleep 0.01; do
    echo
    read -p "Kill ${user}'s processes listed above? [yes/no] " answer
    if [[ "$answer" == "yes" ]]; then
      echo
      sigterm_then_kill "$user" 3 $pids
      break
    elif [[ "$answer" == "no" ]]; then
      break
    else
      echo "Invalid answer. Must be yes or no."
    fi
  done

  return 0
}
