#!/bin/bash
#
# Meant to be exec'ed via:
#
#   alias startx='exec "$JUNK_LIB/startx-exec.sh"'
#
# So that when X dies you're logged out of the linux console tty automatically.
#

. "$JUNK_LIB/enter_to_continue.sh" || exit

function usage() {
  local wm_list=$(get_wm_list)
  echo
  echo "Usage: $0 <window-manager>"
  echo
  echo "Args:"
  echo "  window-manager"
  echo "    The name of the X Window Manager to run."
  echo "    One of: $wm_list"
  # Stop bash from swallowing this blank line when used in die "$(usage)"
  echo " "
}

function ok_die() {
  echo
  echo "$@" >&2
  echo
  enter_to_continue
  exit 1
}

function get_wm_list() {
  local xinit_glob="$HOME/.xinitrc.*"
  declare -g wm_list=""
  for xinit in $(eval ls "$xinit_glob"); do
    local wm=$(basename "$xinit")
    local wm=${wm#.xinitrc.}
    wm_list="${wm_list}$wm "
  done

  if [[ -z "$wm_list" ]]; then
    ok_die "ERROR: No .xinitrc files found matching glob: $xinit_glob"
  fi

  echo "$wm_list"
}

function main() {
  if [[ $# -ne 1 ]]; then
    ok_die "$(usage)"
  fi

  chosen_xinitrc="$HOME/.xinitrc.$1"
  if ! [[ -f "$chosen_xinitrc" ]]; then
    ok_die "ERROR: No such file: $chosen_xinitrc"
  fi

  set -e
  set -x
  rm -f "$HOME/.xinitrc"
  cp -pf "$chosen_xinitrc" "$HOME/.xinitrc"
  exec startx |& tee ~/tmp/startx.out.$(ts)
}

main "$@"
