#!/bin/bash

# die echos all given args to stderr and exits with non-zero status.
function die() {
  echo "$@" >&2
  exit 1
}
