#!/bin/bash

function enter_to_continue() {
  local unused_var=
  echo
  read -p "Press enter to continue." unused_var
  echo
}
