#!/bin/bash

#
# Run the currently executing script under root via sudo.
#
# Must be placed at the start of the script.
#
# Saves the original values of $USER & $HOME in $orig_user & $orig_home.
#
function become_root() {
  if [[ "$EUID" -ne 0 ]]; then
    if [[ "$verbose" ]]; then
      echo "WARNING: I am NOT root. Running myself under sudo." >&2
    fi

    export orig_user="$USER"
    export orig_home="$HOME"
    local preserve="orig_user,orig_home"
    if [[ "$preserve_env" ]]; then
      preserve="$preserve,$preserve_env"
    fi

    exec sudo --preserve-env="$preserve" "$0" "$@"
  fi
}
