#!/bin/bash

#
# Parse flag arguments.
#
# Stops parsing flags when the argument "--" is encountered in "$@".
#
# Usage: parse_flags [flag_specs] -- "$@"
#
#   flag_specs - See also parse_flags. Examples:
#
#     v:verbose  # Variable 'verbose' is set to argument passed to -v.
#     n?dry_run  # Variable 'dry_run' is set to "yes" if -n is specified.
#
# Dependencies:
#   usage
#   die
#
function parse_flags() {
  declare -g dry_run=
  declare -g verbose=

  local -A flag_var
  local -A flag_type
  local flag=
  local getopts_spec=
  while [[ $# -gt 0 ]]; do
    arg=$1; shift
    if [[ "$arg" == "--" ]]; then
      #echo "[DEBUG] parse_flags: End of args ('--') found." >&2
      break
    elif [[ "$arg" =~ ^[a-zA-Z0-9][:?][a-zA-Z0-9_]+$ ]]; then
      #echo "[DEBUG] parse_flags: flag_specs += '$arg'" >&2
      flag=${arg:0:1}
      ftype=${arg:1:1}
      fvar=${arg:2}
      flag_var[$flag]="$fvar"
      flag_type[$flag]="$ftype"

      case "$ftype" in
        '?')
          getopts_spec="${getopts_spec}${flag}"
          ;;
        ':')
          getopts_spec="${getopts_spec}${flag}:"
          ;;
        *)
          die "ERROR: parse_flags:$LINENO: flag '$fvar' has bad flag type" \
              "'$arg' (want '?' or ':')."
          ;;
      esac
    else
      die "ERROR: parse_flags: Bad flag_specs: $arg"
    fi
  done

  #echo "[DEBUG] getopts_spec: '$getopts_spec'" >&2
  while getopts "h$getopts_spec" flag; do
    case "$flag" in
      'h')
        usage
        exit
        ;;
      '?')
        # An error was already printed.
        exit 1
        ;;
      *)
        fvar="${flag_var[$flag]}"
        if [[ -z "$fvar" ]]; then
          die "ERROR: Unhandled flag: $flag"
        fi

        ftype="${flag_type[$flag]}"
        case "$ftype" in
          '?')
            declare -g "$fvar=yes"
            ;;
          ':')
            declare -g "$fvar=$OPTARG"
            ;;
          '*')
            die "ERROR: parse_flags:$LINENO: flag '$fvar' has bad flag type" \
                "'$arg' (want '?' or ':')."
        esac
        ;;
    esac
  done
}
