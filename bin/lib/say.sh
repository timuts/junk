#!/bin/bash

#
# Like echo, but with a timestamp prefix.
#
function say() {
  local ts=$(date '+%m/%d %H:%M:%S')
  echo "$ts]" "$@"
}
