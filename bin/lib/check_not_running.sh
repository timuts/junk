#
# Usage: check_not_running "Descriptive Name" <pgrep_args ...>
#
# With the provided `pgrep` args, checks if any matching processes are running.
# If any are running, calls `exit` after printing some info about the running
# process.
#
# Example:
#   check_not_running "mutt" --full '^mutt\s*$'
#
function check_not_running() {
  local proc_name=$1; shift
	local pids=$(pgrep "$@")
	if [[ -n "$pids" ]]; then
		echo
		echo "# $proc_name is already running:"
		echo
		ps uwwp $pids

    local tty_re=$(
      echo $(
        ps ho tty p $pids \
          | egrep -v '^[?]$' \
          | sed -En 's@/@[/]@p') \
          | tr ' ' '|')
		if [[ -n "$tty_re" ]]; then
			echo
			echo "# Logged on here:"
			echo
      local who_out=$(
        who \
          | awk "/$tty_re/")
			echo "$who_out"

			local screen_tty=$(
        echo "$who_out" \
          | sed -En "s@.*\([^:]*:([^:]+):S\.[0-9].*@\\1@p")
			if [[ -n "$screen_tty" ]]; then
				local screen_pid=$(pgrep -t "$screen_tty" screen)
				if [[ -n "$screen_pid" ]]; then
					echo
					echo "# Under this screen session:"
					echo
					ps uwwp "$screen_pid"
				fi
			fi
		fi
		echo
		exit 1
	fi
}
