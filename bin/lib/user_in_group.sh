#!/bin/bash

#
# Returns success if the specified user is in the specified group.
#
function user_in_group() {
  local user=$1
  local group=$2

  for g in $(id --groups --name "$user"); do
    if [[ "$g" == "$group" ]]; then
      return 0
    fi
  done
  return 1
}
