#!/bin/bash

#
# Run a bluetoothctl command.
#
# Uses $ctlr_mac if set to select a controller.
#
# Dependencies:
#   - apt install bluez
#
# TODO: Support multiple commands.
#
function bt_cmd {
  local flags=()
  while [[ $# -gt 0 && "$1" =~ ^-.* ]]; do
    flags+="$1"
    shift
  done

  local select_cmd=
  if [[ "$ctlr_mac" ]]; then
    select_cmd="select $ctlr_mac"
  fi

  bluetoothctl "${flags[@]}" << __EOT__
$select_cmd
$*
__EOT__
}
