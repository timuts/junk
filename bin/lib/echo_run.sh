#!/bin/bash
#
# echo_run - Echo a command and run it.
#

function echo_run() {
  local argv=()

  echo -n "+ "
  while [[ $# -gt 0 ]]; do
    arg=$1; shift

    if [[ "$arg" =~ [\'"[:space:]"\':!$#*\[\]] ]]; then
      echo -n "'$arg' "
    else
      echo -n "$arg "
    fi

    argv+=("$arg")
  done
  echo

  "${argv[@]}"
}
