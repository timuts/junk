#!/bin/bash

#
# Run a command while teeing its output (stdout + stderr) to the specified
# file. Also appends the exit status to the output file.
#
# The first argument is the output filename and all subsequent arguments are
# the command to execute.
#
# The output filename may contain {} which will be replaced with the complete
# command being executed (after replacing slash with '--' and spaces with
# underscores).
#
# Example:
#
#   # Writes output to "ls_-l.out"
#   run_and_log "{}.out" ls -l
#
function run_and_log() {
  local outfile_tmpl=$1; shift
  local mod_cmd=$(echo "$@" | tr ' ' _ | sed 's@/@--@g')
  local outfile=$(echo "$outfile_tmpl" | sed "s|{}|${mod_cmd}|g")

  ("$@"; echo $'\nEXIT STATUS:' $?) |& tee "$outfile"
  local exit_status=$(
    tail -n1 "$outfile" \
      | sed -n 's/EXIT STATUS: //p')

  if ! [[ "$exit_status" =~ ^[0-9]+$ ]]; then
    exit_status=99
  fi
  return "$exit_status"
}

function run_and_log_ts() {
  local out="${1}.$(date '+%Y-%m-%d-%H%M%S').$$"
  shift
  run_and_log "$out" "$@"
}
