#!/bin/bash

function column_tool() {
  if column --version < /dev/null &> /dev/null; then
    # Assume it's from the util-linux pkg.
    column \
        --table \
        --table-columns BSS,Freq,Signal,SSID \
        --table-right 2,3 \
        --table-order 2,3,1,4 \
        --separator '|'
  else
    # Assume it's from the bsdmainutils pkg.
    column -t
  fi
}

function scan() {
  local wifi_dev=$1

  sudo iw dev "$wifi_dev" scan \
    | sed -En '
        s/^BSS\s*(.*)\(on \w+\).*/;;\1|/p
        s/^\s+(freq|signal|SSID):\s*(.*)/\2|/p
      ' \
    | sed -E '
        s/[^[:print:]]/./g
        s/^(.{40}).+[|]$/\1[...]|/
      ' \
    | tr -d '\n' \
    | sed '
        s/;;/\n/g
        a \\

        q
      ' \
    | sed 's@\s*|\s*@|@g' \
    | sort -t'|' -k3nr \
    | column_tool
}

if [[ "$DO_SCAN" ]]; then
  scan "$1"
fi
