#!/bin/bash
#
# Set up a RAM-backed filesystem which is meant for certain users to store
# temporary files in a slightly more convenient way than /tmp.
#
# This script is meant to be run on startup as root.
#
# Prerequistes:
#
#  1. Add an entry to /etc/fstab. Example (8 GiB, writable by members of group
#     "adm"):
#
#     tmpfs /ram tmpfs nosuid,nodev,user,size=8589934592,mode=1775,gid=adm 0 0
#
#  2. Each user should have a ~/bin/setup-ramfs script which will be run (as
#     them) as soon as /ram is mounted.
#
#  3. Install this this script at /usr/local/libexec/setup-ramfs.sh (it should
#     be owned and writable only by root).
#
#  4. Set it up to be run on system startup. Example for systemd:
#
#     sudo tee /etc/systemd/system/my-ramfs-setup.service << __EOT__
#     [Service]
#     Type=oneshot
#     RemainAfterExit=yes
#     ExecStart=/usr/local/libexec/setup-ramfs.sh
#
#     [Install]
#     WantedBy=multi-user.target
#     __EOT__
#
#     sudo systemctl enable my-ramfs-setup.service
#

if ! mount /ram; then
  echo "ERROR: Failed to mount /ram" >&2
  exit 1
fi

users="tim"

for u in $users; do
  gid=$(
    getent passwd "$u" \
      | cut -d: -f4)

  if ! install -o "$u" -g "$gid" -m 700 -d "/ram/$u"; then
    echo "ERROR: Failed to create /ram/$u." >&2
    exit 1
  fi

  home_dir=$(
    getent passwd "$u" \
      | cut -d: -f6)

  if ! [[ -d "$home_dir" ]]; then
    echo "ERROR: Home dir '$home_dir' for user '$u' does not exist." >&2
    continue
  fi

  cd "/ram/$u"
  sudo -Hu "$u" "$home_dir/bin/setup-ramfs"
done
