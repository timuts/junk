#!/bin/bash

function usage() {
  echo
  echo "Usage: $0 [options] <foo> [-- extra_arg ...]"
  echo
  echo "Arguments:"
  echo "  foo"
  echo "    Description of foo."
  echo
  echo "Options:"
  echo "  -n  Dry-run."
  echo "  -d  Debug mode."
  echo
  echo "Example:"
  echo "   $0 -n blah"
  # Stop bash from swallowing this blank line when used in die "$(usage)"
  echo " "
}

if [[ $# -lt 1 ]]; then
  die "$(usage)"
fi

echo
echo "ERROR: usage-tmpl.sh is not meant to be executed." \
     "It's meant to be a template you copy and modify."
echo
exit 1
