#!/bin/bash

#
# Converts bytes to MiB (mebi-bytes or 2^20).
#
function bytes_to_mib() {
  local bytes=$1

  echo $((bytes / 2**20))
}
