#!/bin/bash

function print_success() {
  status_code=$?
  if [[ "$status_code" -eq 0 ]]; then
    status="SUCCESS"
  else
    status="FAILED"
  fi

  echo
  echo "DONE ($status)."
}
