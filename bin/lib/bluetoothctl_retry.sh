#!/bin/bash

#
# Run a bluetoothctl command until it succeeds or timeout occurs.
#
# Example (give up after 5 tries, which is 5 seconds at minimum):
#   bluetoothctl_retry 5 connect "D0:B8:E3:6F:5D:F3"
#
function bluetoothctl_retry() {
  local secs_left=$1; shift

  local exit_status=1
  while [[ "$secs_left" -gt 0 ]]; do
    let secs_left--
    out=$(bt_cmd "$@" 2>&1)
    exit_status=$?

    if [[ "$exit_status" -ne 0 ]]; then
      say "bt_cmd failed (exit status: $exit_status). Retrying. Output:"
      echo "$out"
      sleep 1
      continue
    fi

    if [[ "$out" =~ Device.*not.available ]]; then
      say "bt_cmd failed (device not available). Retrying."
      sleep 1
      continue
    fi

    break
  done

  return "$exit_status"
}
