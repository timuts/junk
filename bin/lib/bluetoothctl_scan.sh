#!/bin/bash

#
# Scan for a specified device using the bt_cmd function.
#
# Keep trying until it's found.
#
# Example:
#   bluetoothctl_scan "D0:B8:E3:6F:5D:F3" "Brydge C-Type v1.4"
#
# Dependencies:
#   - apt install bluez
#   - $JUNK_LIB/bt_cmd.sh
#
function bluetoothctl_scan() {
  local mac="$1"
  local name="$2"

  say "Turning scan off."
  bt_cmd scan off \
    < /dev/null \
    &> /dev/null

  local timeout_secs=10
  local check_interval=1
  local ts=$(date '+%Y-%m-%d-%H%M%S')
  local scan_out="/tmp/bluetoothctl.scan.on.out.$ts.$$"
  __bluetoothctl_scan_in_bg \
      "$check_interval" "$((timeout_secs / check_interval))" \
      "$scan_out"
  scan_pid=$!
  trap "kill '$scan_pid' 2>/dev/null" EXIT
  say "Scanning started in background (PID: $scan_pid)."

  local secs_left="$timeout_secs"
  local found_it=
  while [[ "$secs_left" -gt 0 ]]; do
    sleep 1 || break
    let secs_left--

    ts=$(date '+%Y-%m-%d-%H%M%S')
    found_it_out="/tmp/bluetoothctl.scan.found_it.out.$ts.$$"
    found_it=$(
      cat "$scan_out".[0-9]* \
        | egrep -i "\s(${mac}\s|${name}\s*$)" \
        > "$found_it_out" \
        && echo "yes")

    [[ "$found_it" ]] && break
    rm "$found_it_out"
  done
  kill "$scan_pid" 2>/dev/null

  if [[ "$found_it" ]]; then
    new_mac=$(
      sed -En 's/.*\s([A-Fa-f0-9]{2}:[A-Fa-f0-9]{2}:\S+).*/\1/p' \
        < "$found_it_out")
    if [[ -n "$new_mac" ]]; then
      mac="$new_mac"
      declare -g mac="$new_mac"
    fi
    say "Found it ($mac)."
    return 0
  fi
  return 1
}

function __bluetoothctl_scan_in_bg() {
  local interval=$1
  local iterations=$2
  local outprefix=$3

  (
    let i=0
    while [[ "$i" -lt "$iterations" ]]; do
      bt_cmd --timeout="$interval" scan on \
        < /dev/null \
        &> "$outprefix.$i"
      let i++
    done
  ) &
}
