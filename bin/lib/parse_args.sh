#!/bin/bash

. "$JUNK_LIB/parse_flags.sh"

#
# Parse command-line arguments.
#
# Usage: parse_args [flag_specs] [arg_names] -- "$@"
#
#   flag_specs - See also parse_flags. Examples:
#
#     v:verbose  # Variable 'verbose' is set to argument passed to -v.
#     n?dry_run  # Variable 'dry_run' is set to "yes" if -n is specified.
#
#   arg_names - Names of variables which receive positional arguments (in
#               order).
#
# Usage example:
#
#   parse_args 'v:verbose' infile -- "$@"
#
# Dependencies:
#   die
#   parse_flags
#   usage        # Must be defined by the caller.
#
function parse_args() {
  declare -a arg_names=("zero-unused")
  declare -a flag_specs=()

  local arg=
  while [[ $# -gt 0 ]]; do
    arg=$1; shift
    if [[ "$arg" == "--" ]]; then
      #echo "[DEBUG] parse_args: End of args ('--') found." >&2
      break
    elif [[ "$arg" =~ ^[a-zA-Z0-9][:?][a-zA-Z0-9_]+$ ]]; then
      #echo "[DEBUG] parse_args: flag_specs += '$arg'" >&2
      flag_specs+=("$arg")
      continue
    elif [[ "$arg" =~ ^[a-zA-Z0-9_]+$ ]]; then
      #echo "[DEBUG] parse_args: arg_names += '$arg'" >&2
      arg_names+=("$arg")
      declare -g "$arg="
    else
      die "ERROR: parse_args: Bad flag_specs or arg_names: $arg"
    fi
  done

  declare -g ARGNUM=1
  while [[ $# -gt 0 ]]; do
    arg=$1

    if [[ "$arg" == "--help" ]]; then
      usage
      exit
    fi

    if [[ "$arg" == -* ]]; then
      #echo "[DEBUG] parse_args: Found a flag-like argument: $arg" >&2
      OPTIND=1
      parse_flags "${flag_specs[@]}" -- "$@"

      while [[ "$OPTIND" -gt 1 ]]; do
        shift
        let OPTIND--
      done

      continue
    fi

    #echo "[DEBUG] parse_args: ARGNUM $ARGNUM #arg_names ${#arg_names[*]}"
    if [[ "$ARGNUM" -ge "${#arg_names[*]}" ]]; then
      die "ERROR: See -h for help. Extraneous argument: $1"
    fi

    local arg_var=${arg_names[$ARGNUM]}
    #echo "[DEBUG] parse_args: Setting $arg_var=$arg" >&2
    declare -g "${arg_var}=$arg"

    shift
    let ARGNUM++
  done
}
