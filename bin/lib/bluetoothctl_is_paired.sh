#!/bin/bash

#
# Succeeds (returns zero) IFF the device is paired.
#
# Example:
#   bluetoothctl_is_paired "D0:B8:E3:6F:5D:F3" "Brydge C-Type v1.4"
#
function bluetoothctl_is_paired() {
  local mac=$1
  local name=$2

  new_mac=$(
    bt_cmd paired-devices \
      < /dev/null \
      |& awk -v mac="$mac" -v name="$name" '
          $1 == "Device" && ($2 == mac || $0 ~ name) {
            print $2
            exit 0
          }

          END {
            exit 1
          }
        ')
  exit_status=$?

  if [[ -n "$new_mac" ]]; then
    mac="$new_mac"
  fi

  return $exit_status
}
