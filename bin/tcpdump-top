#!/bin/bash
#
# Print the top N tcp connections, using tcpdump.
#

function default_iface() {
  local def_route
  if ! def_route=$(ip r g 1.1.1.1); then
    echo "ERROR: Can't get default route from \`ip r g 1.1.1.1\`" >&2
    exit 1
  fi

  local iface=$(sed -En 's/.*\<dev (\S+)\>.*/\1/p' <<< "$def_route")
  if [[ -z "$iface" ]]; then
    echo "ERROR: Failed to extract default iface from \`ip r g ...\` output: '''$def_route'''" >&2
    exit 1
  fi

  echo "$iface"
}

function main() {
  local nlines=${1:-20}
  local npackets=${2:-500}

  if [[ $# -gt 2 ]]; then
    echo "Usage: tcpdump-top [num_lines [num_packets]]" >&2
    exit 1
  fi

  local iface=$(default_iface)
  sudo tcpdump -pni "$iface" -c "$npackets" 2>/dev/null \
    | awk '
        $2 == "IP" {
          l = gensub(/.* length ([0-9]+).*/, "\\1", 1)

          left  = gensub(/[.]([0-9]+):?$/, ":\\1", "g", $3)
          right = gensub(/[.]([0-9]+):?$/, ":\\1", "g", $5)
          if (left > right) {
            temp = left
            left = right
            right = temp
          }

          k = sprintf("%-21s <-> %-21s", left, right);
          a[k] += int(l)
        }

        END {
          for (k in a) {
            printf("%8d  %s\n", a[k], k)
          }
        }' \
    | sort -n \
    | tail -n"$nlines"
}

main "$@"
